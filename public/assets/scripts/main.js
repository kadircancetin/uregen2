var _currentPage = 0;
var pageItemCount = 50;
var tag = 'all';

var Scripts = {
    init: function () {
        var openButton = $('#open'),
            closeButton = $('#close'),
            mobileMenu = $('.mobile-menu'),
            menu = $('.menu'),
            collapsibleBtn = $('.collapsible-btn');

        openButton.on('click', function () {
            mobileMenu.addClass('open');
        });
        closeButton.on('click', function () {
            mobileMenu.removeClass('open');
        });
        mobileMenu.find('ul').html(menu.html());
        collapsibleBtn.on('click', function () {
            $(this).parent().find('.collapsible-content').slideToggle();
        });
    },
    run: function () {
        Scripts.init();
    },
    getPriceList: function () {
        $.ajax({
            url: '/API/PriceList',
            method: 'GET',
            dataType: 'json',
            success: function (resp) {
                console.log(resp);
            }
        });
    },
    filterCatalogs: function (tag) {
        $('table tbody tr').hide();
        $('table tbody tr.' + tag).slice(_currentPage * pageItemCount, (_currentPage * pageItemCount) + pageItemCount + 1).show();
        var itemCount = $('table tbody tr.' + tag).length;

        var pageCount = itemCount / pageItemCount;
        if (itemCount % pageItemCount > 0) pageCount += 1;
        $('.pagination ul').empty();
        for (var i = 1; i <= pageCount; i++) {
            $('.pagination ul').append('<li><a href="javascript:;">' + i + '</a></li>');
        }
        $('.pagination ul li:eq(0)').addClass('active');
        Scripts.bindPaginationEvents();
        Scripts.paging(1);

    }, paging: function (pageNo) {
        _currentPage = pageNo - 1;
        $('table tbody tr').hide();
        if(tag.length>0)
        $('table tbody tr.' + tag).slice(_currentPage * pageItemCount, (_currentPage * pageItemCount) + pageItemCount).show();
        $('.countTotal').text($('table tbody tr.' + tag).length);

        $('.shownRecords').text(((_currentPage * pageItemCount) + 1) + ' - ' + ((_currentPage * pageItemCount) + $('table tbody tr.' + tag+':visible').length));

    }, bindPaginationEvents: function () {
        $('.pagination a').click(function () {
            $('.pagination ul li').removeClass('active');
            $(this).parent().addClass('active');
            var pageNo = $(this).text();
            Scripts.paging(pageNo);

        });
		
		$('.pagination img.nextPage').click(function () { 
			$('.pagination ul li.active').next().find('a').click();
		});
		
		$('.pagination img.prevPage').click(function () { 
$('.pagination ul li.active').prev().find('a').click();
		});
    }
};
Scripts.init();
Scripts.bindPaginationEvents();
function formValidation(formType)
    {
        var name = $("#NameSurname");
        var email = document.forms["RegForm"]["Email"];
        var phone = document.forms["RegForm"]["Phone"];
        var subject =  document.forms["RegForm"]["Departmant"];
        var company = document.forms["RegForm"]["Company"];
        var feedback = document.forms["RegForm"]["FeedbackType"];
        var note = document.forms["RegForm"]["Message"];
		
		if( formType == "iletisim"){
		if (subject.selectedIndex < 1)
		{
			alert("Lütfen iletişime geçmek istediğiniz departmanı seçiniz.");
			subject.focus();
			return false;
		}}
		

        if (name.val() === "")
        {
            window.alert("Lütfen geçerli bir isim giriniz.");
            name.focus();
            return false;
        }
        if (email.value === "")
        {
            window.alert("Lütfen geçerli bir E-mail adresi giriniz. ");
            email.focus();
            return false;
        }

        if (email.value.indexOf("@", 0) < 0)
        {
            window.alert("Lütfen geçerli bir E-mail adresi giriniz.");
            email.focus();
            return false;
        }

        if (email.value.indexOf(".", 0) < 0)
        {
            window.alert("Lütfen geçerli bir E-mail adresi giriniz.");
            email.focus();
            return false;
        }
		if (company.value === "")
        {
            window.alert("Lütfen geçerli bir firma adı giriniz");
            company.focus();
            return false;
        }
        if (phone.value === "")
        {
            window.alert("Lütfen geçerli bir telefon numarası giriniz.");
            phone.focus();
            return false;
        }
       
        if (feedback.selectedIndex < 1)
        {
            alert("Lütfen geri bildirim türü seçiniz.");
            feedback.focus();
            return false;
        }
		 if (note.value === "")
        {
            window.alert("Lütfen notunuzu giriniz.");
            note.focus();
            return false;
        }

        return true;
}
$(document).ready(function () {
    if (window.location.pathname == '/' || window.location.pathname=='/en')
	revealVideo('video','youtube'); // show lightbox main home page
    $('.filter-list a').click(function () {        
        if (tag.length > 0)
            tag = $(this).data('tag');
        Scripts.filterCatalogs(tag);
    });
    $('.show-menu').click(function () {  
		
        if($( this ).siblings( 'ul' ).css('display') == "none")
        {
            $(this).html("-");
        }
        else
        {
             $(this).html("+");
        }
       $( this ).siblings( 'ul' ).toggle();
    ;
   });

    $('.search-container #Search').keyup(function () {

        var value = $('.search-container #Search').val();
        
        if (value.length >= 2) {
            value = value.toLowerCase();
            $('.pagination').hide();
            $('table tbody tr').hide();
            $.each($('table tr'), function (i, v) {

                if ($(v).children().eq(0).html().toLowerCase().indexOf(value) > -1 || $(v).children().eq(1).html().toLowerCase().indexOf(value) > -1 || $(v).children().eq(2).html().toLowerCase().indexOf(value) > -1 || $(v).children().eq(3).html().toString().toLowerCase().indexOf(value) > -1)
                    $(v).show();
            });
        } else { $('table tbody tr.' + tag).show(); $('.pagination').show(); }

    });

});